#include "Item.h"
#include "Container.h"

std::map<std::pair<int, int>, Container> g_Containers;

Container * Container::get(int type, int id) {
	return &g_Containers[std::make_pair(type, id)];
}

int Container::GetSize() const {
	return m_Items.size();
}
Item * Container::GetAt(int slot) const {
	if (slot < 0 || slot >= GetSize()) {
		return 0;
	}
	return m_Items[slot];
}
Item * Container::Find(std::string name, int nth) const {
	int found = 0;
	for (auto i : m_Items) {
		if ( ! name.compare(i->getName())) {
			if (found++ == nth) {
				return i;
			}
		}
	}
	return 0;
}
int Container::GetSlot(Item * item) const {
	int slot = 0;
	for (auto i : m_Items) {
		if (i == item) {
			return slot;
		}
		slot++;
	}
	return -1;
}
void Container::InitItems() {
	for (auto i : m_Items) {
		i->Ignite();
	}
}
void Container::Add(Item * item) {
	m_Items.push_back(item);
}
void Container::Remove(Item * item) {
	auto i = std::find(m_Items.begin(), m_Items.end(), item);
	if (i != m_Items.end()) {
		m_Items.erase(i);
	}
}
void Container::Remove(int slot) {
	Item * item = GetAt(slot);
	
	if (item) {
		Remove(item);
	}
}

void Container::RemoveAll() {
	for (auto i : m_Items) {
		i->resetContainer();
	}
	m_Items.clear();
}

void Container::Clear() {
	for (auto i : m_Items) {
		i->SafeDelete();
	}
	m_Items.clear();
}