#pragma once

#include "main.h"

class Item;

class Container {
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, unsigned int version)
	{
		ar & m_Items;
	}
	std::deque<Item *> m_Items;

public:

	static Container * get(int type, int id);

	void InitItems();

	int GetSize() const;
	Item * GetAt(int slot) const;
	Item * Find(std::string name, int nth) const;
	int GetSlot(Item * item) const;

	void Add(Item * item);
	void Remove(Item * item);
	void Remove(int slot);

	void RemoveAll();
	void Clear();
};

extern std::map<std::pair<int, int>, Container> g_Containers;