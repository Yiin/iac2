#pragma once
#include <stdio.h>

#include <string>
#include <deque>
#include <stack>
#include <unordered_map>
#include <map>
#include <chrono>
#include <sstream>

#include <algorithm>

#include <fstream>


#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/unordered_map.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/deque.hpp>

#include "Item.h"
#include "Container.h"


typedef void(*logprintf_t)(char* format, ...);

extern logprintf_t logprintf;