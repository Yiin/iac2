#include "main.h"

#include "SDK/sampgdk.h"

logprintf_t logprintf;

extern void *pAMXFunctions;

std::string convertNativeStringToString(AMX *amx, cell input)
{
	char *string = NULL;
	amx_StrParam(amx, input, string);
	return string ? string : ""; // ??
}

cell AMX_NATIVE_CALL n_CreateItem(AMX* amx, cell* params) {
	std::string name = convertNativeStringToString(amx, params[1]);

	if (name.length() > 31) {
		name.assign(name, 0, 31);
	}
	Item * item = new Item(name);

	if (item) {
		return item->getId();
	}

	return 0;
}
cell AMX_NATIVE_CALL n_DeleteItem(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		item->Delete();
		return 1;
	}
	return 0;
}
cell AMX_NATIVE_CALL n_IsItemInContainer(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		if (item->isInContainer()) {
			return 1;
		}
	}
	return 0;
}
cell AMX_NATIVE_CALL n_GetItemContainer(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		if (item->isInContainer()) {
			cell* addr[2] = { NULL, NULL };

			amx_GetAddr(amx, params[2], &addr[0]);
			amx_GetAddr(amx, params[3], &addr[1]);

			*addr[0] = static_cast<cell>(item->getContainerType());
			*addr[1] = static_cast<cell>(item->getContainerId());
		}
	}
	return 0;
}
cell AMX_NATIVE_CALL n_GetItemName(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		cell *text = NULL;
		amx_GetAddr(amx, params[2], &text);
		amx_SetString(text, item->getName().c_str(), 0, 0, static_cast<size_t>(params[3]));
	}
	return 0;
}
cell AMX_NATIVE_CALL n_GetItemInfo(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		std::string key = convertNativeStringToString(amx, params[2]);

		return item->getInfo(key, static_cast<int>(params[3]));
	}
	return 0;
}
cell AMX_NATIVE_CALL n_GetItemInfoUpperBound(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		return item->GetItemInfoUpperBound(static_cast<int>(params[2]));
	}
	return 0;
}
cell AMX_NATIVE_CALL n_GetItemInfoKey(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		int infoIndex = static_cast<int>(params[2]);
		int index = static_cast<int>(params[4]);

		cell *text = NULL;
		amx_GetAddr(amx, params[3], &text);
		amx_SetString(text, item->GetItemInfoKey(infoIndex, index).c_str(), 0, 0, static_cast<size_t>(params[5]));
		
		return 1;
	}
	return 0;
}
cell AMX_NATIVE_CALL n_GetItemInfoValue(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		int infoIndex = static_cast<int>(params[2]);
		int index = static_cast<int>(params[3]);

		return item->GetItemInfoValue(infoIndex, index);
	}
	return 0;
}
cell AMX_NATIVE_CALL n_SetItemInfo(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		std::string key = convertNativeStringToString(amx, params[2]);
		int value = static_cast<int>(params[3]);

		item->setInfo(key, value, static_cast<int>(params[4]));

		return 1;
	}
	return 0;
}

// GetItemInfo_Text(item, const key[], dest[], index = 0, dest_len = sizeof(dest))
cell AMX_NATIVE_CALL n_GetItemInfo_Text(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		std::string key = convertNativeStringToString(amx, params[2]);

		int index = static_cast<int>(params[4]);

		cell *text = NULL;
		amx_GetAddr(amx, params[3], &text);
		amx_SetString(text, item->getInfo_Text(key, index).c_str(), 0, 0, static_cast<size_t>(params[5]));

		return item->getInfo(key, static_cast<int>(params[3]));
	}
	return 0;
}

// SetItemInfo_Text(item, const key[], text[], index = 0)
cell AMX_NATIVE_CALL n_SetItemInfo_Text(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		std::string key = convertNativeStringToString(amx, params[2]);
		std::string text = convertNativeStringToString(amx, params[3]);

		item->setInfo_Text(key, text, static_cast<int>(params[4]));

		return 1;
	}
	return 0;
}

//--
cell AMX_NATIVE_CALL n_GetItemInfo_OtherItem(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		std::string key = convertNativeStringToString(amx, params[2]);

		if (item->getInfo_OtherItem(key)) {
			return item->getInfo_OtherItem(key)->getId();
		}
	}
	return 0;
}
cell AMX_NATIVE_CALL n_SetItemInfo_OtherItem(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		std::string key = convertNativeStringToString(amx, params[2]);
		Item * other_item = g_Items[static_cast<int>(params[3])];
		
		if (other_item) {
			item->setInfo_OtherItem(key, other_item);

			return 1;
		}
		else if (static_cast<int>(params[3]) == 0) {
			item->deleteInfo_OtherItem(key);
		}
	}
	return 0;
}
//--
cell AMX_NATIVE_CALL n_AdjustItemInfo(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		std::string key = convertNativeStringToString(amx, params[2]);
		int value = static_cast<int>(params[3]);
		int index = static_cast<int>(params[4]);

		item->setInfo(key, item->getInfo(key, index) + value, index);

		return item->getInfo(key, index);
	}
	return 0;
}
cell AMX_NATIVE_CALL n_ClearItemInfo(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		int index = static_cast<int>(params[2]);

		item->clearInfo(index);

		return 1;
	}
	return 0;
}
cell AMX_NATIVE_CALL n_DeleteItemInfo(AMX* amx, cell* params) {
	int uiid = static_cast<int>(params[1]);

	Item * item = g_Items[uiid];

	if (item) {
		std::string key = convertNativeStringToString(amx, params[2]);
		int index = static_cast<int>(params[3]);

		item->deleteInfo(key, index);

		return 1;
	}
	return 0;
}
cell AMX_NATIVE_CALL n_Container_Clear(AMX* amx, cell* params) {
	int type = static_cast<int>(params[1]);
	int id = static_cast<int>(params[2]);

	Container::get(type, id)->Clear();

	return 1;
}
cell AMX_NATIVE_CALL n_Container_RemoveAll(AMX* amx, cell* params) {
	int type = static_cast<int>(params[1]);
	int id = static_cast<int>(params[2]);

	Container::get(type, id)->RemoveAll();

	return 1;
}

cell AMX_NATIVE_CALL  n_Container_Save(AMX* amx, cell* params)
{
	int type = (int)params[1];
	int id = (int)params[2];
	std::string dest = convertNativeStringToString(amx, params[3]);
	std::string filename = convertNativeStringToString(amx, params[4]);

	if (dest.back() != '/') {
		dest += "/";
	}
	dest += filename;
	dest += ".dat";

	std::ofstream ofs(dest.c_str());
	boost::archive::text_oarchive oa(ofs);
	oa & (*Container::get(type, id));

	return 1;
}
cell AMX_NATIVE_CALL  n_Container_Load(AMX* amx, cell* params)
{
	int type = (int)params[1];
	int id = (int)params[2];
	std::string src = convertNativeStringToString(amx, params[3]);
	std::string filename = convertNativeStringToString(amx, params[4]);

	if (src.back() != '/') {
		src += "/";
	}
	src += filename;
	src += ".dat";

	std::ifstream ifs(src.c_str());

	if (ifs) {
		boost::archive::text_iarchive ia(ifs);
		ia >> (*Container::get(type, id));
		Container::get(type, id)->InitItems();
	}

	return 1;
}
cell AMX_NATIVE_CALL n_Container_AddItem(AMX* amx, cell* params) {
	int type = static_cast<int>(params[1]);
	int id = static_cast<int>(params[2]);
	int uiid = static_cast<int>(params[3]);

	Item * item = g_Items[uiid];

	if (item) {
		if (item->isInContainer()) {
			item->getContainer()->Remove(item);
		}
		Container::get(type, id)->Add(item);

		item->setContainer(type, id);

		return 1;
	}

	return 0;
}
cell AMX_NATIVE_CALL n_Container_RemoveItem(AMX* amx, cell* params) {
	int type = static_cast<int>(params[1]);
	int id = static_cast<int>(params[2]);
	int uiid = static_cast<int>(params[3]);

	Item * item = g_Items[uiid];

	if (item) {
		if (item->isInContainer()) {
			if (item->getContainer() == Container::get(type, id)) {
				item->getContainer()->Remove(item);

				item->resetContainer();

				return 1;
			}
		}
	}

	return 0;
}
cell AMX_NATIVE_CALL n_Container_CountItems(AMX* amx, cell* params) {
	int type = static_cast<int>(params[1]);
	int id = static_cast<int>(params[2]);

	return Container::get(type, id)->GetSize();
}
cell AMX_NATIVE_CALL n_Container_HasItem(AMX* amx, cell* params) {
	int type = static_cast<int>(params[1]);
	int id = static_cast<int>(params[2]);
	std::string name = convertNativeStringToString(amx, params[3]);

	return Container::get(type, id)->Find(name, 0) != 0;
}
cell AMX_NATIVE_CALL n_Container_FindItem(AMX* amx, cell* params) {
	int type = static_cast<int>(params[1]);
	int id = static_cast<int>(params[2]);
	std::string name = convertNativeStringToString(amx, params[3]);
	int nth = static_cast<int>(params[4]);

	Item * item = Container::get(type, id)->Find(name, nth);

	if (item) {
		return item->getId();
	}

	return 0;
}
cell AMX_NATIVE_CALL n_Container_GetItemAt(AMX* amx, cell* params) {
	int type = static_cast<int>(params[1]);
	int id = static_cast<int>(params[2]);
	int slot = static_cast<int>(params[3]);

	Item * item = Container::get(type, id)->GetAt(slot);

	if (item) {
		return item->getId();
	}
	return 0;
}
cell AMX_NATIVE_CALL n_Container_GetItemSlot(AMX* amx, cell* params) {
	int type = static_cast<int>(params[1]);
	int id = static_cast<int>(params[2]);
	int uiid = static_cast<int>(params[3]);

	Item * item = g_Items[uiid];

	if (item) {
		if (item->isInContainer()) {
			return Container::get(type, id)->GetSlot(item);
		}
	}
	return -1;
}

PLUGIN_EXPORT unsigned int PLUGIN_CALL Supports()
{
	return SUPPORTS_VERSION | SUPPORTS_AMX_NATIVES;
}

PLUGIN_EXPORT bool PLUGIN_CALL Load(void **ppData)
{
	pAMXFunctions = ppData[PLUGIN_DATA_AMX_EXPORTS];
	logprintf = (logprintf_t)ppData[PLUGIN_DATA_LOGPRINTF];

	return true;
}

PLUGIN_EXPORT void PLUGIN_CALL Unload()
{
}

AMX_NATIVE_INFO PluginNatives[] =
{
	{ "CreateItem", n_CreateItem },
	{ "DeleteItem", n_DeleteItem },
	{ "IsItemInContainer", n_IsItemInContainer },
	{ "GetItemContainer", n_GetItemContainer },
	{ "GetItemName", n_GetItemName },
	{ "GetItemInfo", n_GetItemInfo },
	{ "GetItemInfoUpperBound", n_GetItemInfoUpperBound },
	{ "GetItemInfoKey", n_GetItemInfoKey },
	{ "GetItemInfoValue", n_GetItemInfoValue },
	{ "SetItemInfo", n_SetItemInfo },
	{ "SetItemInfo_Text", n_SetItemInfo_Text },
	{ "GetItemInfo_Text", n_GetItemInfo_Text },
	{ "GetItemInfo_OtherItem", n_GetItemInfo_OtherItem },
	{ "SetItemInfo_OtherItem", n_SetItemInfo_OtherItem },
	{ "AdjustItemInfo", n_AdjustItemInfo },
	{ "ClearItemInfo", n_ClearItemInfo },
	{ "DeleteItemInfo", n_DeleteItemInfo },
	{ "Container_Clear", n_Container_Clear },
	{ "Container_Save", n_Container_Save },
	{ "Container_Load", n_Container_Load },
	{ "Container_AddItem", n_Container_AddItem },
	{ "Container_RemoveItem", n_Container_RemoveItem },
	{ "Container_RemoveAll", n_Container_RemoveAll },
	{ "Container_CountItems", n_Container_CountItems },
	{ "Container_HasItem", n_Container_HasItem },
	{ "Container_FindItem", n_Container_FindItem },
	{ "Container_GetItemAt", n_Container_GetItemAt },
	{ "Container_GetItemSlot", n_Container_GetItemSlot },
	{ 0, 0 }
};

PLUGIN_EXPORT int PLUGIN_CALL AmxLoad(AMX *amx)
{
	return amx_Register(amx, PluginNatives, -1);
}


PLUGIN_EXPORT int PLUGIN_CALL AmxUnload(AMX *amx)
{
	return AMX_ERR_NONE;
}