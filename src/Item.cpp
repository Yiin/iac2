#include "Item.h"
#include "Container.h"

std::map<int, Item *> g_Items;
std::map<result_type, Item *> g_HashList;

std::stack<int> g_UnusedUniqueIds;

void Item::Ignite() {
	//logprintf("debug: new item with id %i and name %s", m_Id, m_Name.c_str());
	//logprintf("debug: hash %s", m_hash.c_str());
	//if (g_HashList[m_hash] != this) {
	//	this->SafeDelete();
	//}
	//else {
		g_HashList[m_hash] = this;
	//}
}

void Item::Init() {
	static int nextUniqueId = 1;
	if (g_UnusedUniqueIds.empty()) {
		m_Id = nextUniqueId++;
	}
	else {
		m_Id = g_UnusedUniqueIds.top();
		g_UnusedUniqueIds.pop();
	}

	g_Items[m_Id] = this;

	if ( ! m_Name.empty()) {
		//logprintf("debug: new item with id %i and name %s", m_Id, m_Name.c_str());

		using namespace std::chrono;
		milliseconds ms = duration_cast< milliseconds >(
			system_clock::now().time_since_epoch()
		);
		long long time = ms.count();
		std::stringstream ss;
		ss << time;
		ss << getId();
		m_hash = ss.str();
		logprintf("debug: new hash %s", m_hash.c_str());

		g_HashList[m_hash] = this;
	}
}

void Item::Delete() {
	if (isInContainer()) {
		getContainer()->Remove(this);
	}
	if (g_HashList[m_hash] == this) {
		g_HashList[m_hash] = NULL;
	}
	g_UnusedUniqueIds.push(m_Id);
	g_Items.erase(m_Id);
	delete this;
}

void Item::SafeDelete() {
	if (g_HashList[m_hash] == this) {
		g_HashList[m_hash] = NULL;
	}
	g_UnusedUniqueIds.push(m_Id);
	g_Items.erase(m_Id);
	delete this;
}

bool Item::isInContainer() const {
	return m_IsInContainer;
}

void Item::resetContainer() {
	m_ContainerType = 0;
	m_ContainerID = 0;

	m_IsInContainer = false;
}

void Item::setContainer(int type, int id) {
	m_ContainerType = type;
	m_ContainerID = id;

	m_IsInContainer = true;
}
Container * Item::getContainer() const {
	return &g_Containers[std::make_pair(m_ContainerType, m_ContainerID)];
}
int Item::getContainerType() const {
	return m_ContainerType;
}
int Item::getContainerId() const {
	return m_ContainerID;
}

int Item::GetItemInfoUpperBound(int index) const {
	return m_Info.at(index).size();
}
std::string Item::GetItemInfoKey(int infoIndex, int index) const {
	int count = 0;
	
	for (auto i : m_Info.at(index)) {
		if (count++ == infoIndex) {
			return i.first;
		}
	}
	return "";
}
int Item::GetItemInfoValue(int infoIndex, int index) const {
	int count = 0;

	for (auto i : m_Info.at(index)) {
		if (count++ == infoIndex) {
			return i.second;
		}
	}
	return 0;
}

void Item::deleteInfo(std::string key, int index) {
	m_Info[index].erase(key);
}
void Item::clearInfo(int index) {
	m_Info.erase(index);
}

void Item::deleteInfo_OtherItem(std::string key) {
	m_Info_OtherItem.erase(key);
}
void Item::setInfo_OtherItem(std::string key, Item * item) {
	m_Info_OtherItem[key] = item->getHash();
}
Item * Item::getInfo_OtherItem(std::string key) {
	return g_HashList[m_Info_OtherItem[key]];
}
void Item::setInfo(std::string key, int value, int index) {
	m_Info[index][key] = value;
}
int Item::getInfo(std::string key, int index) {
	return m_Info[index][key];
}

void Item::deleteInfo_Text(std::string key, int index) {
	m_Info_Text[index].erase(key);
}
void Item::setInfo_Text(std::string key, std::string text, int index) {
	m_Info_Text[index][key] = text;
}
std::string Item::getInfo_Text(std::string key, int index) {
	return m_Info_Text[index][key];
}

int Item::getId() const {
	return m_Id;
}

std::string Item::getName() const {
	return m_Name;
}

bool Item::operator == (const Item * r) {
	return r->getId() == getId();
}

Item::Item() {
	Init();
}
Item::Item(std::string name) {
	m_Name.assign(name, 0, 31);

	Init();
}
Item::~Item() {

}