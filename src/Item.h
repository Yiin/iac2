#pragma once

#include "main.h"

typedef std::string result_type;

class Container;

class Item {
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive & ar, unsigned int /* version */)
	{
		ar & m_Name;
		ar & m_hash;
		ar & m_Info;
		ar & m_Info_OtherItem;
		ar & m_IsInContainer;
		ar & m_ContainerType;
		ar & m_ContainerID;
		ar & m_Info_Text;
	}

	int m_Id;

	std::string m_Name;

	std::map<int, std::unordered_map<std::string, int>> m_Info;
	std::map<int, std::unordered_map<std::string, std::string>> m_Info_Text;
	std::unordered_map<std::string, result_type> m_Info_OtherItem;

	bool m_IsInContainer;

	int m_ContainerType;
	int m_ContainerID;

	result_type m_hash;

public:

	void Ignite();
	void Init();
	void Delete();
	void SafeDelete();

	result_type getHash() const { return m_hash; }

	int getId() const;

	std::string getName() const;

	bool isInContainer() const;

	void resetContainer();
	void setContainer(int type, int id);
	Container * getContainer() const;

	int getContainerType() const;
	int getContainerId() const;

	int GetItemInfoUpperBound(int index) const;
	std::string GetItemInfoKey(int infoIndex, int index) const;
	int GetItemInfoValue(int infoIndex, int index) const;

	void deleteInfo(std::string key, int index);
	void clearInfo(int index);
	void deleteInfo_OtherItem(std::string key);
	void setInfo_OtherItem(std::string key, Item * item);
	Item * getInfo_OtherItem(std::string key);
	void setInfo(std::string key, int value, int index);
	int getInfo(std::string key, int index);


	void deleteInfo_Text(std::string key, int index);
	void setInfo_Text(std::string key, std::string text, int index);
	std::string getInfo_Text(std::string key, int index);

	bool operator == (const Item * r);

	Item();
	Item(std::string name);
	~Item();
};

extern std::map<int, Item *> g_Items;
extern std::map<result_type, Item *> g_HashList;